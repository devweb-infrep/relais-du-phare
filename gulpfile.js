const { src, watch, series } = require("gulp");
const browserSync = require("browser-sync").create();

function serve(cb) {
  browserSync.init({
    server: {
      baseDir: "./"
    }
  });

  cb();
}

function reload(cb) {
  browserSync.reload();
  cb();
}

function css(cb) {
  console.log("CSS TASK");
  src("**/*.css").pipe(browserSync.stream());
  cb();
}

module.exports.default = series(serve, function(cb) {
  watch("**/*.css", css);
  watch("index.html", reload);
  watch("**/*.js", reload);

  cb();
});
